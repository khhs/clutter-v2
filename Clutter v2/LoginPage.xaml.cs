﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Clutter_v2
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        private void ButtonClick_Login(object sender, RoutedEventArgs e)
        {
            /*User user = new User() { UserName = "kalle", Password = "123", Person = new Person() { FirstName = "Karl Henrik", SurName = "Skaug", Email = "Kalle@mail.no" } };
            await UserSource.PostUserAsync(user);
            User LoginUser = new User() { UserName = TxtUserName.Text, Password = TxtPassword.Password };
            User RetrievedUser = await UserSource.GetUserAsync(LoginUser.UserName);
            if (RetrievedUser != null)
            {
                if (RetrievedUser.Password == LoginUser.Password)
                {
                    this.NavigationService.Navigate(new MainPage(RetrievedUser));
                }
            }*/
            this.NavigationService.Navigate(new MainPage());
        }
    }
}
