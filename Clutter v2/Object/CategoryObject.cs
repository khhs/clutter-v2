﻿using Clutter.Object;
using DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clutter_v2.Object
{
    public class CategoryObject
    {
        public Category Category { get; set; }
        public ICollection<FileObject> Files { get; set; }

        public CategoryObject()
        {
            Files = new HashSet<FileObject>();
        }
    }
}
