﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Clutter.Object
{
    public class FileObject
    {
        public String DisplayName { get; set; }
        public String InstallLocation { get; set; }
        public String FullPath { get; set; }
        public BitmapSource Thumbnail { get; set; }
    }
}
