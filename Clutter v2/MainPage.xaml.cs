﻿using Clutter.DataSource;
using Clutter.Object;
using DataModel;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Clutter_v2
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        private ObservableCollection<Category> Categories;
        private ObservableCollection<Program> Programs;
        private ObservableCollection<FileObject> Files;
        private CategoryDataSource CategorySource;
        private ProgramDataSource ProgramSource;
        private RegistryManager RegManager;

        public MainPage()
        {
            InitializeComponent();

            CategorySource = new CategoryDataSource();
            ProgramSource = new ProgramDataSource();
            RegManager = new RegistryManager();
            Categories = new ObservableCollection<Category>();
            Programs = new ObservableCollection<Program>();
            Files = new ObservableCollection<FileObject>();

            TxtUserName.Text = "Admin";

            if (Categories.Count >= 20)
            {
                BtnAddCategory.IsEnabled = false;
            }
            SetupCategoryList();
            //SampleDate();
        }

        private async void SetupCategoryList()
        {
            Categories = await CategorySource.GetCategories();
            if (Categories.Count == 0)
                return;
            ListViewCategories.DataContext = Categories;
            LoadApplicationList(Categories.FirstOrDefault(c => c.CategoryId == 1));
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            //SetupCategoryList();
        }

        private void ListViewCategoryItem_Click(object sender, MouseButtonEventArgs e)
        {
            LoadApplicationList(Categories.FirstOrDefault(c => c.CategoryId == ListViewCategories.SelectedIndex + 1));
        }

        private void LoadApplicationList(Category category)
        {
            if (category != null)
            {
                var a = category.Programs;
                ListViewApps.DataContext = a;
            }
        }



        private void SampleDate()
        {
            Category cat = new Category();
            cat.Programs.Add(new Program() { Path = @"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe" });
            cat.Programs.Add(new Program() { Path = @"C:\\Program Files (x86)\\Dropbox\\Client\\Dropbox.exe" });

            ListViewCategories.DataContext = cat;

            foreach (Program pro in cat.Programs)
            {

                ShellProperties properties = ShellFile.FromFilePath(pro.Path).Properties;
                Byte[] bytes = new Byte[16];
                BitConverter.GetBytes(3).CopyTo(bytes, 0);
                var a = properties.GetProperty(new PropertyKey("0cef7d53-fa64-11d1-a203-0000f81fedee", 3)).ValueAsObject;

                FileObject file = new FileObject();
                file.FullPath = pro.Path;
                file.DisplayName = (String)a;
                file.Thumbnail = ShellFile.FromFilePath(pro.Path).Thumbnail.BitmapSource;

                Files.Add(file);
            }

            ListViewApps.DataContext = Files;
        }



        private void PlussButton_MouseHoverEnter(object sender, MouseEventArgs e)
        {
            Image Img = ((Image)sender);
            ImageSource logo = new BitmapImage(new Uri("pack://application:,,,/Assets/plus-icon-hover.png"));
            Img.Source = logo;
        }

        private void PlussButton_MouseHoverLeave(object sender, MouseEventArgs e)
        {
            Image Img = ((Image)sender);
            ImageSource logo = new BitmapImage(new Uri("pack://application:,,,/Assets/plus-icon.png"));
            Img.Source = logo;
        }

        private void CategoryText_EnterPressed(object sender, KeyEventArgs e)
        {
            var a = ListViewCategories.SelectedIndex;

            var b = 0;
        }

        private async void PlussButton_MouseClick(object sender, MouseButtonEventArgs e)
        {
            Category cat = new Category() { CategoryName = "All" };
            cat.Programs.Add(new Program() { Path = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe", ProgramName = "Google Chrome" });
            cat.Programs.Add(new Program() { Path = @"C:\Program Files (x86)\Dropbox\Client\Dropbox.exe", ProgramName = "Dropbox" });
            await CategorySource.PostCategoryAsync(cat);
            SetupCategoryList();
        }

        private async void FolderSearchButton_MouseClick(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog FileDialog = new OpenFileDialog();
            FileDialog.Filter = "Executable files (.exe)|*.exe";
            Boolean? PressedOkey = FileDialog.ShowDialog();
            if (PressedOkey == true)
            {
                ShellProperties properties = ShellFile.FromFilePath(FileDialog.FileName).Properties;
                Byte[] bytes = new Byte[16];
                BitConverter.GetBytes(3).CopyTo(bytes, 0);
                var a = properties.GetProperty(new PropertyKey("0cef7d53-fa64-11d1-a203-0000f81fedee", 3)).ValueAsObject;

                FileObject file = new FileObject();
                file.FullPath = FileDialog.FileName;
                file.DisplayName = (String)a;
                file.Thumbnail = ShellFile.FromFilePath(FileDialog.FileName).Thumbnail.BitmapSource;

                Program app = new Program();
                app.ProgramName = file.DisplayName;
                app.Path = file.FullPath;

                Category cat = Categories.FirstOrDefault(c => c.CategoryId == 1);

                cat.Programs.Add(app);

                await CategorySource.PutCategoryAsync(cat);

                LoadApplicationList(cat);
            }
        }

        private void SampleDateBtnClick(object sender, MouseButtonEventArgs e)
        {
            var a = ListViewApps.DataContext;
            //System.Diagnostics.Process.Start([ListViewApps.SelectedIndex]);
        }
    }
}
