﻿using Clutter.Object;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Shell;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace Clutter_v2
{
    public class RegistryManager
    {
        List<FileObject> Files;

        public RegistryManager()
        {
            Files = new List<FileObject>();
        }

        public List<FileObject> GetComputerApps()
        {
            SearchApps();
            return Files;
        }

        private void GetThumbnails(FileObject file)
        {
            //String[] tmp = Directory.GetFiles(file.InstallLocation, @"*.exe", SearchOption.AllDirectories);
            //if(tmp[0]!=null)
                //file.Thumbnail = ShellFile.FromFilePath(tmp[0]).Thumbnail.Bitmap;
        }

        private void SearchForApps()
        {
            string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
            {
                foreach (string subkey_name in key.GetSubKeyNames())
                {
                    using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                    {
                        FileObject file = new FileObject();
                        file.DisplayName = subkey.GetValue("DisplayName") + "";
                        file.InstallLocation = subkey.GetValue("InstallLocation") + "";
                        if (file.DisplayName != "")
                        {
                            if (file.InstallLocation != "")
                            {
                                GetThumbnails(file);
                                Files.Add(file);
                            }
                        }
                    }
                }
            }
        }

        private void SearchApps()
        {
            /*ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_Product");
            foreach (ManagementObject mo in mos.Get())
            {
                Console.WriteLine(mo["Name"]);
            }*/
        }


    }
}
