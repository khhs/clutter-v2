﻿using DataModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Clutter.DataSource
{
    public class UserDataSource
    {
        /*private HttpClient ServiceClient;
        private ObservableCollection<User> _users = new ObservableCollection<User>();

        public UserDataSource()
        {
            ServiceClient = new HttpClient();
            ServiceClient.BaseAddress = new Uri(@"http://clutterwebapi.azurewebsites.net/api/");
        }

        public async Task<User> GetUserAsync(String userName)
        {
            HttpResponseMessage Response = await ServiceClient.GetAsync("Users/" + userName);
            if (Response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<User>(await Response.Content.ReadAsStringAsync());
            }
            return null;
        }

        private async Task GetUsersAsync()
        {
            HttpResponseMessage Response = await ServiceClient.GetAsync("Users");
            if (Response.IsSuccessStatusCode)
            {
                _users = JsonConvert.DeserializeObject<ObservableCollection<User>>(await Response.Content.ReadAsStringAsync());
            }
        }

        public async Task<User> PostUserAsync(User user)
        {
            StringContent Content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
            HttpResponseMessage Response = await ServiceClient.PostAsync("Users", Content);
            if (Response.IsSuccessStatusCode)
            {
                User RetrievedUser = JsonConvert.DeserializeObject<User>(await Response.Content.ReadAsStringAsync());
                _users.Add(RetrievedUser);
                return RetrievedUser;
            }
            return null;
        }

        public async Task<User> PutUserAsync(User user)
        {
            StringContent Content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
            HttpResponseMessage Response = await ServiceClient.PutAsync("Users/" + user.UserName, Content);
            if (Response.IsSuccessStatusCode)
            {
                User RetrievedUser = JsonConvert.DeserializeObject<User>(await Response.Content.ReadAsStringAsync());
                User tmp = _users.First(u => u.UserName == RetrievedUser.UserName);
                tmp = RetrievedUser;
                return RetrievedUser;
            }
            return null;
        }
        */
    }
}