﻿using Clutter.Object;
using Clutter_v2.Object;
using DataModel;
using Microsoft.WindowsAPICodePack.Shell;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Clutter.DataSource
{
    class CategoryDataSource
    {
        private HttpClient ServiceClient;
        private ObservableCollection<CategoryObject> Categories = new ObservableCollection<CategoryObject>();

        public CategoryDataSource()
        {
            ServiceClient = new HttpClient();
            ServiceClient.BaseAddress = new Uri(@"http://clutterwebapi.azurewebsites.net/api/");
        }

        public async Task<CategoryObject> GetCategoryAsync(String categoryName)
        {
            HttpResponseMessage Response = await ServiceClient.GetAsync("Categories/" + categoryName);
            if (Response.IsSuccessStatusCode)
            {
                Category cat =  JsonConvert.DeserializeObject<Category>(await Response.Content.ReadAsStringAsync());
                CategoryObject cato = new CategoryObject() { Category = cat };
                foreach(Program p in cat.Programs)
                {
                    cato.Files.Add(CreateFileObject(p));
                }
                return cato;
            }
            return null;
        }

        public async Task<ObservableCollection<CategoryObject>> GetCategories()
        {
            if (Categories.Count == 0)
                await GetCategoriesAsync();
            return Categories;
        }

        private async Task GetCategoriesAsync()
        {
            HttpResponseMessage Response = await ServiceClient.GetAsync("Categories");
            if (Response.IsSuccessStatusCode)
            {
                foreach(Category cat in JsonConvert.DeserializeObject<List<Category>>(await Response.Content.ReadAsStringAsync()))
                {
                    CategoryObject cato = new CategoryObject() { Category = cat };
                    foreach (Program p in cat.Programs)
                    {
                        cato.Files.Add(CreateFileObject(p));
                    }
                    Categories.Add(cato);
                }
            }
        }

        public async Task<Category> PostCategoryAsync(Category category)
        {
            StringContent Content = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");
            HttpResponseMessage Response = await ServiceClient.PostAsync("Categories", Content);
            if (Response.IsSuccessStatusCode)
            {
                Category cat = JsonConvert.DeserializeObject<Category>(await Response.Content.ReadAsStringAsync());
                Categories.Add(cat);
                return cat;
            }
            return null;
        }

        public async Task PutCategoryAsync(Category category)
        {
            StringContent Content = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");
            HttpResponseMessage Response = await ServiceClient.PutAsync("Categories/" + category.CategoryId, Content);
            if (Response.IsSuccessStatusCode)
            {
            }
        }

        private FileObject CreateFileObject(Program program)
        {
            FileObject file = new FileObject();
            file.FullPath = program.Path;
            file.DisplayName = program.ProgramName;
            file.Thumbnail = ShellFile.FromFilePath(program.Path).Thumbnail.BitmapSource;
            return file;
        }
    }
}
