﻿using Clutter.Object;
using DataModel;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Clutter.DataSource
{
    public class ProgramDataSource
    {
        private HttpClient ServiceClient;
        private ObservableCollection<FileObject> Programs = new ObservableCollection<FileObject>();

        public ProgramDataSource()
        {
            ServiceClient = new HttpClient();
            ServiceClient.BaseAddress = new Uri(@"http://clutterwebapi.azurewebsites.net/api/");
        }

        public async Task<Program> GetProgramAsync(String programName)
        {
            HttpResponseMessage Response = await ServiceClient.GetAsync("Programs/" + programName);
            if (Response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<Program>(await Response.Content.ReadAsStringAsync());
            }
            return null;
        }

        private FileObject CreateFileObject(Program program)
        {
            FileObject file = new FileObject();
            file.FullPath = program.Path;
            file.DisplayName = program.ProgramName;
            file.Thumbnail = ShellFile.FromFilePath(program.Path).Thumbnail.BitmapSource;
            return file;
        }

        public async Task<ObservableCollection<FileObject>> GetPrograms()
        {
            //if (Programs.Count == 0)
                await GetProgramsAsync();
            return Programs;
        }

        private async Task GetProgramsAsync()
        {
            HttpResponseMessage Response = await ServiceClient.GetAsync("Programs");
            if (Response.IsSuccessStatusCode)
            {
                foreach(Program pro in JsonConvert.DeserializeObject<ObservableCollection<Program>>(await Response.Content.ReadAsStringAsync()))
                {
                    Programs.Add(CreateFileObject(pro));
                }
            }
        }

        public async Task<Program> PostProgramAsync(Program program)
        {
            StringContent Content = new StringContent(JsonConvert.SerializeObject(program), Encoding.UTF8, "application/json");
            HttpResponseMessage Response = await ServiceClient.PostAsync("Programs", Content);
            if (Response.IsSuccessStatusCode)
            {
                Program prog = JsonConvert.DeserializeObject<Program>(await Response.Content.ReadAsStringAsync());
                Programs.Add(CreateFileObject(prog));
                return prog;
            }
            return null;
        }

        public async Task PutProgramAsync(Program program)
        {
            StringContent Content = new StringContent(JsonConvert.SerializeObject(program), Encoding.UTF8, "application/json");
            HttpResponseMessage Response = await ServiceClient.PutAsync("Programs/" + program.ProgramId, Content);
            if (Response.IsSuccessStatusCode)
            {

            }
        }
    }
}
