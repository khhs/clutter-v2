﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class Program
    {
        public int ProgramId { get; set; }
        public String ProgramName { get; set; }
        public String Path { get; set; }
        public int TimesStarted { get; set; }
        public Boolean Compatible { get; set; }
        public long LastTimeUsed { get; set; }
        public virtual ICollection<Category> Categories { get; set; }

        public Program()
        {
            Categories = new HashSet<Category>();
        }

    }

    public class Category
    {
        public int CategoryId { get; set; }
        public String CategoryName { get; set; }

        public virtual ICollection<Program> Programs { get; set; }

        public Category()
        {
            Programs = new HashSet<Program>();
        }
    }
}
