﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Program> Programs { get; set; }
        public DbSet<Category> Categories { get; set; }

        public DatabaseContext()
            : base(@"Data Source=clutterdatabase.database.windows.net; Initial Catalog=ClutterDatabase; User ID=''; Password='';")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        //: base(@"Data Source=donau.hiof.no; Initial Catalog=khskaug; User ID=''; Password='';")
    }
}
